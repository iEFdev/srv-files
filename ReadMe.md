# srv-files

![][license]

Some of my server files I use in OS X _(local dev srv)_. Feel free to grab and use anything. You'll have to adjust the paths to match your own installation, of course.

- - -

#### Apache
- `apachectl` - a slightly modified version of the standard file. I (sometimes) use `links`, and have another set of paths to the status and info pages. There's also a _“dumprun”_ option.
- `geoipupd` - Update script for 2 data base files for GeoIP. One could perhaps set up an Launchdaemon or something, but I run that one from time to time manually.


#### PHP
- `fpmctl` - Based on `apachectl` to work in a simular way to start/stop/restart etc.
- `fpmctl70` - Based on `apachectl` to work in a simular way to start/stop/restart etc.
- `php-local` - Update/download script for my local PHP manual pages.
- `php-dl` - Downloading a tarball + key.
- `php-vers` - Script to add a custom version _(`PHP_EXTRA_VERSION`)_.

About `fpmctl70`... The name is only to separate them. Copy the file to/as:

	/usr/local/php70/sbin/fpmctl

…or where your PHP7 installation are.


#### MariaDB/MySQL
- `…`


#### Postgres
- `postgres.server` - Based on MacPorts `postgresql95-server.wrapper`. To use manuallyand with my own paths.


#### Gogs

_Testing Gogs at the moment…_

- `gogsctl` - Small script to start/stop/restart Gogs, using the Launchdaemon.



#### LaunchDaemons

`/System/Library/LaunchDaemons`

- `org.apache.httpd.plist`
- `net.php.php-fpm.plist` - _PHP 5.6_
- `net.php.php7-fpm.plist` - _PHP 7.0_
- `net.php.php-local.plist` - _Running every Saturday morning._

`/Library/LaunchDaemons`

- `io.gogs.web.plist` - _For Gogs - Go Git Service._


 <!-- Markdown link & img dfn's -->
[license]: https://img.shields.io/badge/License-0BSD-789.svg?style=plastic "BSD Zero Clause License (SPDX: 0BSD)"
